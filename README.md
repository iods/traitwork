# traitWork ReadMe
* License: GNU GPL v3.0
* Last update: 9/11/2014
* State: WIP


### Table of Content
* 1 Project Details
    * 1.1 Description
    * 1.2 Contributors

### 1.1 Description
traitWork is a trait based PHP framework, aiming for a more flexible workflow than similar frameworks.

### 1.2 Contributors
* #### CoreTeam
    * Daniel Lieberwirth - Project Lead - daniellieberwirth@gmail.com