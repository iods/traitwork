<?php

namespace traitWork\Examples;
// Namespace = Path

/**
 * ExampleTrait
 *
 * @package traitWork
 * @category Examples
 * @version Pre-Alpha
 *
 * @author Daniel Lieberwirth <daniellieberwirth@gmail.com>
 *
 * @license http://www.gnu.org/licenses/ GNU GPL v3.0
 * @copyright (c) 2014, traitWork Team
 *
 * @todo FooBar
 */
trait ExampleTrait {
// Traitname = Filename

	/**
	 * Flag-Container
	 * @var int
	 */
	protected $cFlags = 0;

	/**
	 * Checks if a Flag is set, i.e. if the Flag is true.
	 * @param int $flag Flag(s)
	 * @return bool <b>True:</b> If Flag(s) set<br>
	 *				<b>False:</b> Otherwise
	 * @throws \InvalidArgumentException
	 */
	public function isFlagSet($flag) {
		if(!is_int($flag) and !is_long($flag))
			throw new \InvalidArgumentException('Argumnet is not an integer.');
		return (($this->cFlags & $flag) == $flag);
	}

	/**
	 * Sets Flag(s) to a specific value, i.e. true or false.
	 * @param int $flag Flag(s)
	 * @param bool $value Value
	 * @throws \InvalidArgumentException
	 */
	public function setFlag($flag, $value) {
		if(!is_int($flag) and !is_long($flag))
			throw new \InvalidArgumentException('Argument #1 is not an integer.');
		if(!is_bool($value))
			throw new \InvalidArgumentException('Argument #2 is not a boolean.');
		if($value)
			$this->cFlags |= $flag;
		else
			$this->cFlags &= ~$flag;
	}

}
