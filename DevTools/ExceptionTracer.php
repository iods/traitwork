<?php

namespace traitWork\DevTools;

/**
 * ExceptionTracer
 *
 * @package traitWork
 * @category DevTools
 * @version Pre-Alpha
 *
 * @author Daniel Lieberwirth <daniellieberwirth@gmail.com>
 *
 * @license http://www.gnu.org/licenses/ GNU GPL v3.0
 * @copyright (c) 2014, traitWork Team
 *
 * @todo UnitTest
 */
trait ExceptionTracer {

	/**
	 * Trace Title
	 * @var string
	 */
	protected static $exTitle = 'ExceptionTracer';
	/**
	 * Trace Note
	 * @var string
	 */
	protected static $exNote = 'traitWork ExceptionTracer';

	/**
	 * Trace Body Template path (optional)
	 * @var string
	 */
	protected static $exBody;
	/**
	 * Trace Item Template path (optional)
	 * @var string
	 */
	protected static $exItem;

	protected static function traceEx(\Exception $ex) {

		// Tracer Body
		if(isset(static::$exBody) and file_exists(static::$exBody)) {
			$b = file_get_contents(static::$exBody);
		} else {
			$b = '<!doctype html><html><head><meta charset="utf-8"/><title>{$title}</title><style>::selection{background:rgba(0,0,0,.3);text-shadow:none}html{font-family:"Liberation Mono" ,monospace;font-size:16px}body{margin:0;padding:0;font-size:62.5%;color:#333;background-color:#f5f5f5}section{margin:0 auto;margin-top:16px;width:650px;border-radius:5px;color:#e0e2e4;background-color:#293134;box-sizing:border-box;box-shadow:inset 0 0 30px rgba(0,0,0,.9);overflow:hidden}section>header{padding:2px;padding-top:6px;border-top-left-radius:5px;border-top-right-radius:5px;font-size:24px;font-weight:bold;text-align:center;color:#e2aa36;background-color:#2f393c;border-bottom:rgba(255,255,255,.05) 1px solid;text-shadow:4px 4px 20px rgba(0,0,0,.9);box-shadow:0 0 20px rgba(0,0,0,.9);box-sizing:border-box}section>article{margin:0;padding:10px;font-size:14px;border-top:rgba(0,0,0,.2) 1px solid;border-bottom:rgba(0,0,0,.2) 1px solid}section>footer{padding:4px;border-bottom-left-radius:5px;border-bottom-right-radius:5px;text-align:center;color:#66747b;background-color:#2f393c;box-shadow:0 0 20px rgba(0,0,0,.9);border-top:rgba(255,255,255,.05) 1px solid;box-sizing:border-box}.attribute{color:#b3b689}.background{color:#293134}.comment{color:#66747b}.constant{color:#678cb1}.default{color:#e0e2e4}.error{color:#a32828}.highlight{color:#848c94}.identifier{color:#e0e2e4}.keyword{color:#93c763}.number{color:#ffcd22}.operator{color:#e8e2bd}.separator{color:#e8e2bd}.string{color:#ec7600}.tag{color:#8cbbad}.vairable{color:#678cb1}.whitespace{color:#394448}article.exception{opacity:.65;transition:opacity linear .1s}article.exception:hover{opacity:1;cursor:pointer}article.exception>header{font-size:15px}article.exception .message{padding:7px;padding-left:14px;padding-right:14px;font-size:13px;color:#d39745}article.exception>footer{font-size:12px}</style></head><body><section><header>{$title}</header> {$content} <footer>{note}</footer></section></body></html>';
		}

		// TracerItem
		if(isset(static::$exItem) and file_exists(static::$exItem)) {
			$i = file_get_contents(static::$exItem);
		} else {
			$i = '<article class="exception"><header><span class="error">{$exception}</span><span class="default">: </span><span class="vairable">{$class}</span><span class="separator">{$type}</span><span class="identifier">{$function}</span><span class="number"> {$code}</span></header><div class="message">{$message}</div><footer><span class="highlight">In </span><span class="comment">{$file}</span><span class="highlight"> at Line </span><span class="number">{$line}</span></footer></article>';
		}

		// Parsing
		foreach(self::_u($ex) as $e) {
			$c[] = self::_p($i, self::_n($e));
		}
		$c = array(
			'title'		=> static::$exTitle,
			'content'	=> join($c),
			'note'		=> static::$exNote
		);
		$c = self::_p($b, $c);
		return str_replace(getcwd(), '', $c);

	}

	/**
	 * Generates an info-array from an exception.
	 * @param \Exception $ex
	 * @return array Info-Array
	 */
	private static function _n(\Exception $ex) {
		if(!($t = $ex->getTrace())
		|| in_array($t[0]['function'], array('require', 'require_once', 'include', 'include_once'))) {
			return array(
				'exception'		=> get_class($ex),
				'message'		=> $ex->getMessage(),
				'file'			=> $ex->getFile(),
				'line'			=> strval($ex->getLine())
			);
		}
		$t = $t[0];
		return array(
			'exception'			=> get_class($ex),
			'message'			=> $ex->getMessage(),
			'code'				=> strval($ex->getCode()),
			'file'				=> isset($t['file']) ? $t['file'] : $ex->getFile(),
			'line'				=> isset($t['line']) ? strval($t['line']) : strval($ex->getLine()),
			'class'				=> isset($t['class']) ? $t['class'] : '',
			'type'				=> isset($t['type']) ? $t['type'] : '',
			'function'			=> isset($t['function']) ? $t['function'].'()' : ''
		);
	}

	/**
	 * Basic template parser.
	 * @param string $tmpl Template-String
	 * @param array $cont Content-Array
	 */
	private static function _p($tmpl, array $cont) {
		foreach($cont as $k => $v)
			$tmpl = str_ireplace('${'.$k.'}', $v, $tmpl);
		return $tmpl;
	}

	/**
	 * Unstacks an ExceptionStack.
	 * @param \Exception $ex
	 * @return array Exception-Array
	 */
	private static function _u(\Exception $ex) {
		$r = array($ex);
		$t = $ex;
		while(($t = $t->getPrevious()))
			$r[] = $t;
		return $r;
	}

}
